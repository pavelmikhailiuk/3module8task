package iterator.main;

import java.util.Iterator;
import java.util.List;

import iterator.list.impl.LIFOIterableArrayList;

public class Runner {
	public static void main(String[] args) {
		List<String> strings = new LIFOIterableArrayList<>();
		strings.add("first");
		strings.add("second");
		strings.add("third");
		strings.add("fourth");
		strings.set(0, "fifth");
		strings.set(1, "sixth");
		Iterator<String> stringsIterator = strings.iterator();
		while (stringsIterator.hasNext()) {
			System.out.println(strings);
			System.out.println(stringsIterator.next());
			stringsIterator.remove();
		}
	}
}
