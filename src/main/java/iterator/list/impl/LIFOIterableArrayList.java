package iterator.list.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LIFOIterableArrayList<E> extends ArrayList<E> {

	private static final long serialVersionUID = 1L;

	private ArrayList<Integer> elementsOrder = new ArrayList<>();

	public LIFOIterableArrayList() {
		super();
	}

	public LIFOIterableArrayList(Collection<? extends E> c) {
		super(c);
	}

	public LIFOIterableArrayList(int initialCapacity) {
		super(initialCapacity);
	}

	@Override
	public E set(int index, E element) {
		elementsOrder.remove(Integer.valueOf(index));
		elementsOrder.add(Integer.valueOf(index));
		return super.set(index, element);
	}

	@Override
	public void add(int index, E element) {
		super.add(index, element);
		elementsOrder.add(Integer.valueOf(index));
	}

	@Override
	public boolean add(E e) {
		elementsOrder.add(size());
		return super.add(e);
	}

	@Override
	public Iterator<E> iterator() {
		return new LIFOIteratorImpl();
	}

	private class LIFOIteratorImpl implements Iterator<E> {
		ArrayList<Integer> iterationOrder = LIFOIterableArrayList.this.elementsOrder;
		int cursor = iterationOrder.size() - 1;
		int lastRet = -1;
		int expectedModCount = modCount;

		@Override
		public boolean hasNext() {
			return cursor >= 0;
		}

		@Override
		@SuppressWarnings("unchecked")
		public E next() {
			checkForComodification();
			int currentIndex = iterationOrder.get(cursor);
			if (currentIndex >= size())
				throw new NoSuchElementException();
			Object[] elementData = toArray();
			if (currentIndex >= elementData.length)
				throw new ConcurrentModificationException();
			lastRet = cursor;
			cursor = cursor - 1;
			return (E) elementData[currentIndex];
		}

		@Override
		public void remove() {
			if (lastRet < 0)
				throw new IllegalStateException();
			checkForComodification();
			try {
				int index = iterationOrder.get(lastRet);
				LIFOIterableArrayList.this.remove(index);
				iterationOrder.remove(lastRet);
				shiftIndexes(index);
				lastRet = -1;
				expectedModCount = modCount;
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}
		}

		final void checkForComodification() {
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
		}

		final void shiftIndexes(int index) {
			for (int i = 0; i < iterationOrder.size(); i++) {
				Integer idx = iterationOrder.get(i);
				if (idx > index) {
					iterationOrder.set(i, idx - 1);
				}
			}
		}
	}

}
